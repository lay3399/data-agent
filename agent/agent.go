package agent

import (
	"code.simon.com/data-agent/common"
	"code.simon.com/data-agent/plugins/es"
	"code.simon.com/data-agent/plugins/gotail"
	"code.simon.com/data-agent/plugins/kafka"
	"errors"
)

// FactoryInterface FactoryInterface工厂接口
type FactoryInterface interface {
	GetSource(option string) WorkerInterface
	GetTarget(option string) WorkerInterface
}

type Factory struct{}

func (factory *Factory) GetSource(option string) WorkerInterface {
	switch option {
	case "tail":
		return gotail.GetTailTaskMsg()
	case "kafka":
		return kafka.GetTaskManager()
	default:
		common.ManualHandleError("未指定source", errors.New("未指定source"))
	}
	return nil
}

func (factory *Factory) GetTarget(option string) WorkerInterface {
	switch option {
	case "kafka":
		return kafka.GetTaskManager()
	case "tail":
		return gotail.GetTailTaskMsg()
	case "es":
		return es.GetTaskManager()
	default:
		common.ManualHandleError("未指定target", errors.New("未指定target"))
	}
	return nil
}

// NewWorkerFactory 创建一个新的source
func NewWorkerFactory() *Factory {
	return &Factory{}
}

// WorkerInterface  工人接口
type WorkerInterface interface {
	InitSource()
	InitTarget()
}
