package main

import (
	"code.simon.com/data-agent/agent"
	"code.simon.com/data-agent/conf"
	"sync"
)

func main() {
	// 初始化ini文件配置
	conf.GetConfigObj()
	// 配置中心处理
	conf.ConfigHandler()
	// 得到一个工厂
	factory := agent.NewWorkerFactory()
	// 初始化target
	factory.GetTarget(conf.ConfigObj.Base.DataTarget).InitTarget()
	// 初始化source
	factory.GetSource(conf.ConfigObj.Base.DataSource).InitSource()
	wg := sync.WaitGroup{}
	wg.Add(1)
	wg.Wait()
}
