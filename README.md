# data-agent

## 一、简介

​		简称“数据代理”，实现的初始目的是用go语言实现ELK中和logstash一样收集、解析和转换日志的工具。logstash的性能问题比它的替代者来比的话还是差了一些。Filebeat采用了go语言开发，它重构了logstash采集器源码，性能比logstash更优秀。学习了go语言的基础语法，自己便动手去实现一个自己的日志搜集工具。在日志搜集的功能上去自定义一些额外的功能，不仅仅是日志搜集，比如：实现任意两个组件的数据同步转换的功能、统一的配置中心等等。该项目使用了简单工厂，策略等设计模式，用来简单了解go也是一个不错的项目。

​		apache2.0 协议。

## 二、入门

### 2.1 经典案例

![image-20211018100115232](https://gitee.com/lay3399/img/raw/master/image-20211018100115232.png)

### 2.2 配置文件

base域中，通过data_source和data_target来指定数据的来源及流向。可选参数为：其他ini文件域名称。`->`符号用来指定流向其他组件的key,这个key可能是topic

```ini
# data-agent: 日志代理
[base]
data_source = tail
data_target = es
channel_size = 100000
config_src = local

# tail 本地日志搜集插件
[tail]
logfile_path = D:\Temp\a2.log->web_log


[es]
target_ip = http://127.0.0.1:9200
target_index = log
target_type = log
target_delimiter =
target_format = IP-MSG

# kafka的配置
# 当kafka既是数据源又是目的地时可以这样配置
#target_ip = 192.168.38.150:9092->192.168.38.150:9092
[kafka]
address = 192.168.38.150:9092
topic = web_log
channel_size = 100000
source_ip = 192.168.38.150:9092
source_topic = web_log
target_ip = 192.168.38.150:9092
target_topic = web_log2

[nacos]
# 命名空间
NamespaceId = 50374f8b-45ac-47a3-a7ad-580b21b76847
# 所有节点共用配置文件
FocusConfig = false
TimeoutMs = 5000
NotLoadCacheAtStart = true
LogDir = /tmp/nacos/log
CacheDir = /tmp/nacos/cache
RotateTime = 1h
MaxAge = 3
LogLevel = debug
ipAdders = http://106.75.245.165:8848//nacos
DataId = logagent-config-config.json
Group = data-agent


```



## 三、核心概念

这部分主要用来理解代码。

### 3.1角色介绍

#### 3.1.1 Source

源，什么源？数据来源。数据从那里而来。

#### 3.1.2 Target

目标，数据又往何处而去。

### 3.3 包的关系

![image-20211018101208139](https://gitee.com/lay3399/img/raw/master/image-20211018101208139.png)

**plugins**

各个插件，比如kafka、es，都得实现Initsource和InitTarget方法，agent包里来调度各个plugins

### 3.4 实现方式

source将数据流向一个统一channel,各个组件读channel.