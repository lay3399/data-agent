package conf

import (
	"code.simon.com/data-agent/model"
	"github.com/go-ini/ini"
	"github.com/sirupsen/logrus"
	"sync"
)


var ConfigObj *model.Config

var once sync.Once

// LoadProperties 加载配置文件
func LoadProperties() *model.Config {
	// 1. 加载配置文件
	once.Do(func() {
		ConfigObj = new(model.Config)
		err := ini.MapTo(ConfigObj, "./conf/config.ini")
		if err != nil {
			logrus.Error("load config failed,err:%v", err)
			panic(err)
		}
		logrus.Printf("load config.ini success %v", ConfigObj)
	})

	return ConfigObj
}

//GetConfigObj 获取全局配置参数对象
func GetConfigObj() *model.Config {
	return LoadProperties()
}


