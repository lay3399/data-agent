package conf

import "code.simon.com/data-agent/common"

// ConfigHandler 处理配置文件的配置信息
func ConfigHandler() {
	common.MidChannel = make(chan *common.MidChannelModel, ConfigObj.Base.ChanSize)
	// 1. 处理日志文件配置
	switch ConfigObj.Base.ConfigSrc {
	case "file":
		break
	case "etcd":
	// do something
	case "nacos":
		// 配置中心初始化
		InitNacos()
	default:
		break
	}
}
