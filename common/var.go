package common

type MidChannelModel struct {
	Value  *[]byte
	Key  string
}

var MidChannel chan *MidChannelModel
