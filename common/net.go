package common

import (
	"net"
	"strings"
)

func GetLocalIp() string  {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	HandleError(err)
	defer conn.Close()
	addr := conn.LocalAddr().(*net.UDPAddr)
	return strings.Split(addr.IP.String(),":")[0]

}
