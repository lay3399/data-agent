package common

import (
	"errors"
	"github.com/sirupsen/logrus"
)

// HandleError 处理异常
func HandleError(err error)  {
	if err != nil {
		logrus.Error(err)
		panic(err)
	}
}
// ManualHandleError 手动处理错误
func ManualHandleError(info string,err error)  {
	if err != nil {
		logrus.Error("%s,%s",info,err)
		panic(err)
	}
}

// HandleNewError 处理一个新的错误
func HandleNewError(info string)  {
	logrus.Error("%s",info)
	panic(errors.New(info))
}