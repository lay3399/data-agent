package model

// Config 全局配置结构体
type Config struct {
	Base        `ini:"base"`  // 基础配置
	TailConfig  `ini:"tail"`  // tail配置
	KafkaConfig `ini:"kafka"` // kfk配置
	NacosConfig `ini:"nacos"` // nacos配置
	EsConfig    `ini:"es"`    // es配置
}

// KafkaConfig kafka配置结构体
type KafkaConfig struct {
	SourceIp    string `ini:"source_ip"`
	SourceTopic string `ini:"source_topic"`
	TargetIp    string `ini:"target_ip"`
	TargetTopic string `ini:"target_topic"`
}

// NacosConfig Nacos配置结构体
type NacosConfig struct {
	FocusConfig         bool   `ini:"FocusConfig"`
	NamespaceId         string `ini:"NamespaceId"`
	TimeoutMs           uint64 `ini:"TimeoutMs"`
	NotLoadCacheAtStart bool   `ini:"NotLoadCacheAtStart"`
	LogDir              string `ini:"LogDir"`
	CacheDir            string `ini:"CacheDir"`
	RotateTime          string `ini:"RotateTime"`
	MaxAge              int64  `ini:"MaxAge"`
	LogLevel            string `ini:"LogLevel"`
	IpAdders            string `ini:"ipAdders"`
	DataId              string `ini:"DataId"`
	Group               string `ini:"Group"`
}

// Base  基础配置
type Base struct {
	DataSource string `ini:"data_source"`
	ChanSize   int64  `ini:"channel_size"`
	DataTarget string `ini:"data_target"`
	ConfigSrc  string `ini:"config_src"`
}

// EsConfig EsConf
type EsConfig struct {
	SourceIp        string `ini:"source_ip"`
	SourceIndex     string `ini:"source_ip"`
	SourceType      string `ini:"source_type"`
	SourceDelimiter string `ini:"source_delimiter"`
	SourceFormat    string `ini:"source_format"`
	TargetIp        string `ini:"target_ip"`
	TargetIndex     string `ini:"target_index"`
	TargetType      string `ini:"target_type"`
	TargetDelimiter string `ini:"target_delimiter"`
	TargetFormat    string `ini:"target_format"`
}

// TailConfig  tail插件配置
type TailConfig struct {
	LogFilePath string `ini:"logfile_path"`
}
